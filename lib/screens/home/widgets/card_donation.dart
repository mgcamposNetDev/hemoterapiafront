import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/models/donation.dart';
import 'package:flutter_hemoterapia/screens/home/widgets/item_donation.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';

class CardDonations extends StatelessWidget {
  List<Donation> donations;

  CardDonations({@required this.donations});

  @override
  Widget build(BuildContext context) {

    final listItemsDonations = ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return Divider(
          height: 1,
          color: Colors.white24,
        );
      },
      shrinkWrap: false,
      itemCount: donations.length,
      itemBuilder: (BuildContext ctx, int index) {
        return ItemDonation(
          donation: donations[index],
        );
      },
    );

    final containerWelcome = Container(
      margin: EdgeInsets.only(
        left: 36,
        right: 36,
      ),
      height: 56.0 * donations.length,
      constraints: BoxConstraints(
        maxHeight: 56.0 * 3 - 1,
      ),
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
      decoration: BoxDecoration(
        color: ThemeColors.primary,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black38,
            blurRadius: 8,
            offset: Offset(0.0, 1),
          )
        ],
      ),
      child: listItemsDonations,
    );

    return containerWelcome;
  }
}
