import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/models/person.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';

class CardWelcome extends StatelessWidget {
  Person userdata;
  int totalDonations;
  String nextDonation;

  CardWelcome({@required this.totalDonations, @required this.userdata, this.nextDonation });

  @override
  Widget build(BuildContext context) {
    final nameText = RichText(
      text: new TextSpan(
        text: userdata.name + " " + userdata.surname,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w700,
          fontSize: 18,
          fontFamily: 'Raleway',
        ),
      ),
    );

    final document = RichText(
      text: new TextSpan(
        text: userdata.document.toString(),
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w700,
          fontSize: 15,
          fontFamily: 'Raleway',
        ),
      ),
    );

    final bloodTypeText = RichText(
      text: new TextSpan(
        text: userdata.bloodType,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w700,
          fontSize: 15,
          fontFamily: 'Raleway',
        ),
      ),
    );

    final counterDonationText = totalDonations > 0
        ? RichText(
            text: new TextSpan(
              text: totalDonations.toString() +
                  " donaciones realizadas",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w200,
                fontFamily: 'Raleway',
              ),
            ),
          )
        : Container();


    final nextDontationText = totalDonations > 0
        ? RichText(
            text: new TextSpan(
              text: "Podés realizar tu próxima donación el ${nextDonation}",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w200,
                fontFamily: 'Raleway',
              ),
            ),
          )
        : Container();
        
    final containerWelcome = Container(
      margin: EdgeInsets.only(
        left: 36,
        right: 36,
      ),
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
      decoration: BoxDecoration(
          color: ThemeColors.primary,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38, blurRadius: 8, offset: Offset(0.0, 1))
          ]),
      child: Column(
        children: <Widget>[nameText, document, bloodTypeText, counterDonationText],
      ),
    );

    return containerWelcome;
  }
}
