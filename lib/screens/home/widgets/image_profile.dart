import 'package:flutter/material.dart';

class ProfileImage extends StatelessWidget {
  String path;
  ProfileImage(this.path);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 120.0,
      height: 120.0,
      decoration: new BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.grey[100],
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 8,
          ),
        ],
      ),
      child: ClipOval(
        child: FadeInImage.assetNetwork(
          placeholder: "assets/images/default-profile.png",
          fit: BoxFit.cover,
          image: path,
        ),
      ),
    );
  }
}
