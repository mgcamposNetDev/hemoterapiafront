import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';
import 'package:flutter_svg/svg.dart';

class BackgroundCircles extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return Container(
      height: 220,
      width: double.infinity,
      color: ThemeColors.secondary,
      child: Transform.scale(
        alignment: Alignment(0, 0.14),
        scale: 1.8,
        child: new SvgPicture.asset(
          'assets/images/fondo-circulos.svg',
          semanticsLabel: 'A red up arrow',
          fit: BoxFit.contain,
          allowDrawingOutsideViewBox: false,
          height: 220,
          color: Colors.white,
        ),
      ),
    );
  }
}
