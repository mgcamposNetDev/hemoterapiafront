import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/models/donation.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';
import 'package:meta/meta.dart';

class ModalDonationDetail extends StatelessWidget {
  final Donation donation;

  const ModalDonationDetail({@required this.donation});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Donación del " + donation.date.replaceAll("-", "/"),
            textAlign: TextAlign.left,
            style: TextStyle(
              color: ThemeColors.white,
              fontWeight: FontWeight.w700,
              fontFamily: 'Raleway',
              fontSize: 14,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Text(
            donation.quantity > 1
                ? """Se realizaron ${donation.quantity} donaciones en ${donation.place} que resultaron ${(donation.capable ? "aptas" : "no aptas")}"""
                : """Se realizó una donación en ${donation.place} que resultó ${(donation.capable ? "apta" : "no apta")}""",
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w400,
              fontFamily: 'Raleway',
              fontSize: 14,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: OutlineButton(
                  highlightedBorderColor: ThemeColors.white,
                  onPressed: () => Navigator.pop(context),
                  color: ThemeColors.white,
                  textColor: ThemeColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "Cerrar",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: 'Raleway',
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: 16,
              ),
              Expanded(
                child: FlatButton(
                  onPressed: () => {},
                  color: ThemeColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  padding: EdgeInsets.symmetric(vertical: 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "Compartir",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: ThemeColors.primary,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Raleway',
                            fontSize: 14,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
