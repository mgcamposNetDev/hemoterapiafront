import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/authentication/authentication_bloc.dart';
import 'package:flutter_hemoterapia/blocs/authentication/authentication_state.dart';
import 'package:flutter_hemoterapia/blocs/login/login_bloc.dart';
import 'package:flutter_hemoterapia/repository/user_repository.dart';
import 'package:flutter_hemoterapia/screens/login/login_form.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meta/meta.dart';

class LoginScreen extends StatelessWidget {
  final UserRepository userRepository;
  LoginScreen({Key key, @required this.userRepository})
      : assert(userRepository != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: BlocProvider(
          create: (context) => LoginBloc(
            userRepository: userRepository,
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
          ),
          child: SingleChildScrollView(
            child: Center(
              child: Padding(
                padding: EdgeInsets.only(top: 140),
                child: LoginView(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class LoginView extends StatefulWidget {
  @override
  LoginViewState createState() => LoginViewState();
}

class LoginViewState extends State<LoginView> {
  final _formKey = GlobalKey<FormState>();

  @protected
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (context, state) {
        if (state is AuthenticationAuthenticated) {
          Navigator.of(context).pushNamed('/home');
        }
        if (state is AuthenticationAuthenticatedFirstTime) {
          Navigator.of(context).pushNamed('/walkthrough');
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _logoView,
          SizedBox(height: 50),
          LoginForm(formKey: _formKey),
        ],
      ),
    );
  }

  Widget get _logoView {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50,
            width: 180,
            child: new SvgPicture.asset(
              'assets/images/logo.svg',
              semanticsLabel: 'logo',
            ),
          ),
          SizedBox(height: 12),
          Text(
            'CENTRO REGIONAL',
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 20,
              letterSpacing: -0.5,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            'DE HEMOTERAPIA',
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 20,
              letterSpacing: -0.5,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
