import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/login/login_bloc.dart';
import 'package:flutter_hemoterapia/blocs/login/login_event.dart';
import 'package:flutter_hemoterapia/blocs/login/login_state.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({
    Key key,
    @required GlobalKey<FormState> formKey,
  })  : _formKey = formKey,
        super(key: key);

  final GlobalKey<FormState> _formKey;

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    _onLoginButtonPressed() {
      BlocProvider.of<LoginBloc>(context).add(
        LoginButtonPressed(
          email: _emailController.text,
          password: _passwordController.text,
        ),
      );
    }

    void _validateInputs() {
      if (widget._formKey.currentState.validate()) {
        widget._formKey.currentState.save();
        _onLoginButtonPressed();
      } else {
        setState(() {
          _autoValidate = true;
        });
      }
    }

    return BlocListener<LoginBloc, LoginState>(listener: (context, state) {
      if (state is LoginFailure) {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: Text('${state.error}'),
          ),
        );
      }
    }, child: BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return Form(
          autovalidate: _autoValidate,
          key: widget._formKey,
          child: Container(
            width: 250,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: TextFormField(
                    style: TextStyle(
                      fontFamily: 'Raleway',
                    ),
                    decoration: InputDecoration(hintText: 'Correo electrónico'),
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.emailAddress,
                    controller: _emailController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingrese su correo electrónico';
                      }
                      var regex = RegExp(
                          r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
                      if (!regex.hasMatch(value)) {
                        return 'Correo electrónico invalido';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 16),
                  child: TextFormField(
                    style: TextStyle(
                      fontFamily: 'Raleway',
                    ),
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(hintText: 'Contraseña'),
                    obscureText: true,
                    controller: _passwordController,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Ingrese su contraseña';
                      }
                      return null;
                    },
                  ),
                ),
                RaisedButton(
                  textColor: Theme.of(context).primaryColor,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(35.0),
                  ),
                  elevation: 0,
                  highlightElevation: 0,
                  disabledElevation: 0,
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: state is! LoginLoading
                      ? Text(
                          'Ingresar',
                          style: TextStyle(
                            fontFamily: 'Raleway',
                            fontWeight: FontWeight.w600,
                          ),
                        )
                      : SizedBox(
                          height: 20,
                          width: 20,
                          child: CircularProgressIndicator(
                            backgroundColor:
                                Theme.of(context).primaryColorLight,
                            strokeWidth: 2,
                          ),
                        ),
                  onPressed: () =>
                      state is! LoginLoading ? _validateInputs() : null,
                ),
                InkWell(
                  child: Text(
                    "¿Olvidó su contraseña?",
                    style: TextStyle(
                      height: 2,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/recovery_user');
                  },
                ),
                SizedBox(
                  height: 40,
                ),
                // FacebookButton(),
                RaisedButton(
                  color: Colors.transparent,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(35.0),
                  ),
                  elevation: 0,
                  highlightElevation: 0,
                  disabledElevation: 0,
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    'Registrarse',
                    style: TextStyle(
                        fontFamily: 'Raleway', fontWeight: FontWeight.w600),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/register');
                  },
                )
              ],
            ),
          ),
        );
      },
    ));
  }
}
