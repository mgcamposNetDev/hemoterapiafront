import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class FacebookButton extends StatelessWidget {
  const FacebookButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      textColor: Theme.of(context).primaryColor,
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(35.0),
      ),
      elevation: 0,
      highlightElevation: 0,
      disabledElevation: 0,
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 20,
              child: new SvgPicture.asset(
                  'assets/images/facebook.svg',
                  semanticsLabel: 'facebook'),
            ),
            Text(
              'Ingresar con Facebook',
              style: TextStyle(
                  fontFamily: 'Raleway',
                  fontWeight: FontWeight.w600),
            ),
          ]),
      onPressed: () {
        print("Login button");
      },
    );
  }
}
