import 'dart:io';

import 'package:camera_camera/camera_camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/settings/settings_bloc.dart';
import 'package:flutter_hemoterapia/blocs/settings/settings_event.dart';
import 'package:flutter_hemoterapia/blocs/settings/settings_state.dart';
import 'package:flutter_hemoterapia/screens/widgets/circle_picture.dart';
import 'package:flutter_hemoterapia/screens/widgets/custom_radiogroup.dart';
import 'package:flutter_hemoterapia/screens/widgets/datepicker.dart';
import 'package:flutter_hemoterapia/screens/widgets/progress.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';
import 'package:flutter_hemoterapia/themes/input.dart';
import 'package:flutter_svg/svg.dart';

import '../widgets/custom_dropdown.dart' as custom;

class SettingsScreen extends StatefulWidget {
  SettingsScreen();

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  String urlImage = '';
  String _pathPicture = '';
  String _dateBirth;
  String _name;
  String _document;
  String _weight;
  String _bloodType;
  List<RadioItem> _sexItems = List.unmodifiable([
    RadioItem(label: "Femenino", value: 'f'),
    RadioItem(label: "Masculino", value: 'm'),
  ]);
  final RadioController _sexController = RadioController();
  bool _autoValidate = false;

  List<custom.DropdownMenuItem> _bloodTypeMenuItem;
  final List _bloodTypes =
      List.unmodifiable(["A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-"]);

  @override
  initState() {
    _bloodTypeMenuItem = _getBloodTypeMenuItem;
    BlocProvider.of<SettingsBloc>(context)..add(SettingsFetch());
    super.initState();
  }

  Future<void> _onAddPhoto() async {
    File file = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Camera(
          orientationEnablePhoto: CameraOrientation.portrait,
          imageMask: CameraFocus.circle(
            color: Colors.black.withOpacity(0.5),
          ),
          mode: CameraMode.normal,
        ),
      ),
    );

    setState(() {
      _pathPicture = file != null ? file.uri.toFilePath() : '';
    });
  }

  List<custom.DropdownMenuItem> get _getBloodTypeMenuItem {
    List<custom.DropdownMenuItem> items = new List();
    for (String bloodType in _bloodTypes) {
      items.add(
        new custom.DropdownMenuItem(
          value: bloodType,
          child: new Text(
            "Grupo " + bloodType,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontFamily: 'Raleway'),
          ),
        ),
      );
    }
    return items;
  }

  void _onSave(BuildContext context) {
    print(_bloodType);
    BlocProvider.of<SettingsBloc>(context)
      ..add(SettingsSave(
          bloodType: _bloodType,
          dateBirth: _dateBirth,
          document: _document,
          name: _name,
          weigth: _weight,
          sex: _sexController.value,
          surname:
              "", //TODO: Falta resolver como manejar a nivel de diseño el cambio de nombre y apellido
          profileUrl:
              "" //TODO: Resolver como se va a subir, en que momento vas a tener la URL.
          ));
  }

  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;

    void _validateInputs(BuildContext context) {
      if (formKey.currentState.validate()) {
        formKey.currentState.save();
        _onSave(context);
      } else {
        setState(() {
          _autoValidate = true;
        });
      }
    }

    return BlocListener<SettingsBloc, SettingsState>(
      listener: (context, state) {
        if (state is SettingsSaving) {
          Scaffold.of(context).showSnackBar(
          SnackBar(
            content: Text("Configuración guardada"),
          ),
        );
        }
      },
      child: BlocBuilder<SettingsBloc, SettingsState>(
        builder: (context, state) {
          if (state is SettingsLoading || state is SettingsUninitialized) {
            return ProgressLogo();
          } else {
            return Container(
              height: screen.height,
              color: ThemeColors.white,
              child: SingleChildScrollView(
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: <Widget>[
                    Container(
                      height: 220,
                      width: screen.width,
                    ),
                    Transform.scale(
                      scale: 2,
                      child: new SvgPicture.asset(
                          'assets/images/fondo-circulos.svg',
                          semanticsLabel: 'A red up arrow',
                          fit: BoxFit.fitHeight,
                          allowDrawingOutsideViewBox: true,
                          height: 200,
                          color: Theme.of(context).primaryColor),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: screen.height,
                        width: 300,
                        child: Column(
                          children: <Widget>[
                            buildCardForm(context, state),
                            SizedBox(
                              height: 16,
                            ),
                            RaisedButton(
                              textColor: ThemeColors.white,
                              color: ThemeColors.primary,
                              disabledColor: ThemeColors.primaryLight,
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(35.0),
                              ),
                              elevation: 0,
                              highlightElevation: 0,
                              disabledElevation: 0,
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: state
                                      is SettingsSaving //TODO: falta la forma de poder mostrar la carga sin destruir todo el widget
                                  ? SizedBox(
                                      height: 20,
                                      width: 20,
                                      child: CircularProgressIndicator(
                                        backgroundColor:
                                            Theme.of(context).primaryColorLight,
                                        strokeWidth: 2,
                                      ),
                                    )
                                  : Text(
                                      'Guardar cambios',
                                      style: TextStyle(
                                          fontFamily: 'Raleway',
                                          fontWeight: FontWeight.w600),
                                    ),
                              onPressed: state is! SettingsSaving ? () => _validateInputs(context) : null,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  Widget buildCirclePhoto(SettingsState state) {
    if (state is SettingsInitilized &&
        urlImage != state.personalData.profileUrl) {
      urlImage = state.personalData.profileUrl ?? '';
    }

    return Stack(
      children: <Widget>[
        new Container(
          margin: EdgeInsets.fromLTRB(0, 35, 0, 8),
          width: 250,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 130,
                width: 130,
                child: _pathPicture.isEmpty
                    ? ClipOval(
                        child: FadeInImage(
                            placeholder:
                                AssetImage("assets/images/default-profile.png"),
                            fit: BoxFit.cover,
                            image: new NetworkImage(urlImage)),
                      )
                    : CirclePicture(
                        path: _pathPicture,
                        isUrl: false,
                      ),
              )
            ],
          ),
        ),
        Positioned(
          top: 120,
          right: 50,
          child: FloatingActionButton(
            onPressed: _onAddPhoto,
            elevation: 4,
            child: Icon(
              Icons.edit,
              size: 22,
            ),
            mini: true,
            backgroundColor: ThemeColors.secondary,
            foregroundColor: ThemeColors.white,
            highlightElevation: 3,
          ),
        )
      ],
    );
  }

  Widget buildCardForm(BuildContext context, SettingsState state) {
    return Form(
      key: formKey,
      autovalidate: _autoValidate,
      child: Column(
        children: <Widget>[
          buildCirclePhoto(state),
          Card(
            color: ThemeColors.white,
            elevation: 0,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
                    child: IntrinsicHeight(
                      child: TextFormField(
                        initialValue: state is SettingsInitilized
                            ? state.personalData.name
                            : null,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: ThemeColors.primary,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Raleway',
                          fontSize: 20,
                        ),
                        decoration: transparentInputTheme(
                          labelText: "Nombre",
                          placeholder: "Nombre",
                          color: ThemeColors.primaryLight,
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor, ingrese su nombre';
                          }
                          return null;
                        },
                        onSaved: (value) => _name = value,
                      ),
                    ),
                  ),
                  RichText(
                    text: new TextSpan(
                      text: "ID 40185",
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.w200,
                        fontSize: 12,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Card(
            elevation: 0,
            color: ThemeColors.white,
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 8, 0, 8),
                    child: IntrinsicHeight(
                      child: TextFormField(
                        enableSuggestions: true,
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        initialValue: state is SettingsInitilized
                            ? state.personalData.document.toString()
                            : null,
                        style: TextStyle(
                          color: ThemeColors.primary,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Raleway',
                          fontSize: 14,
                        ),
                        decoration: outlineInput(
                          labelText: "Documento",
                          placeholder: "Documento",
                          prefixIcon: Icons.edit,
                          prefixColor: ThemeColors.secondary,
                          color: ThemeColors.primary,
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor, ingrese su documento';
                          }
                          return null;
                        },
                        onSaved: (value) => _document = value,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 8, 0, 8),
                    child: IntrinsicHeight(
                      child: DatePicker(
                        style: TextStyle(
                          color: ThemeColors.primary,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Raleway',
                          fontSize: 14,
                        ),
                        textAlign: TextAlign.center,
                        initialValue: state is SettingsInitilized
                            ? state.personalData.dateBirth
                            : null,
                        decoration: outlineInput(
                          labelText: "Fecha de nacimiento",
                          placeholder: "Fecha de nacimiento",
                          prefixIcon: Icons.edit,
                          prefixColor: ThemeColors.secondary,
                          color: ThemeColors.primary,
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor, ingrese su fecha de nacimiento';
                          }
                          return null;
                        },
                        onSaved: (value) => _dateBirth = value,
                      ),
                    ),
                  ),
                  CustomRadioGroup(
                    items: _sexItems,
                    controller: _sexController,
                    title: "Sexo",
                    orientation: CustomRadioOrientation.horizontal,
                    style: TextStyle(
                      color: ThemeColors.primary,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Raleway',
                      fontSize: 14,
                    ),
                    color: ThemeColors.secondary,
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 8, 0, 8),
                    child: IntrinsicHeight(
                      child: TextFormField(
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: ThemeColors.primary,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Raleway',
                          fontSize: 14,
                        ),
                        keyboardType: TextInputType.number,
                        initialValue: state is SettingsInitilized
                            ? state.personalData.weigth
                            : null,
                        decoration: outlineInput(
                            labelText: "Peso",
                            placeholder: "Peso",
                            prefixIcon: Icons.edit,
                            prefixColor: ThemeColors.secondary,
                            color: ThemeColors.primary,
                            suffixText: "KG"),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor, ingrese su peso';
                          }
                          return null;
                        },
                        onSaved: (value) => _weight = value,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 8, 0, 8),
                    child: IntrinsicHeight(
                      child: custom.DropdownButtonFormField(
                        initialValue: state is SettingsInitilized
                            ? state.personalData.bloodType
                            : null,
                        value: _bloodType,
                        items: _bloodTypeMenuItem,
                        textAlign: TextAlign.center,
                        buttonStyle: TextStyle(
                          color: ThemeColors.primary,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Raleway',
                          fontSize: 14,
                        ),
                        decoration: outlineInput(
                          labelText: "Tipo de sangre",
                          placeholder: "Tipo de sangre",
                          prefixIcon: Icons.edit,
                          prefixColor: ThemeColors.secondary,
                          color: ThemeColors.primary,
                        ),
                        onChanged: (value) =>
                            setState(() => _bloodType = value),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor, seleccione su tipo de sangre';
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
