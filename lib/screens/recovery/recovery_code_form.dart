import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/recoverycode/recovery_code.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';

class RecoveryCodeForm extends StatefulWidget {
  @override
  _RecoveryCodeFormState createState() => _RecoveryCodeFormState();
}

class _RecoveryCodeFormState extends State<RecoveryCodeForm> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _codeController = TextEditingController();

  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    _onSubmitButtonPressed() {
      BlocProvider.of<RecoveryCodeBloc>(context)
          .add(RecoveryCodeButtonPressed(code: _codeController.text));
    }

    void _validateInputs() {
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        _onSubmitButtonPressed();
      } else {
        setState(() {
          _autoValidate = true;
        });
      }
    }

    final inputCode = Container(
      child: TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 6,
        maxLengthEnforced: true,
        buildCounter: (
          BuildContext context, {
          int currentLength,
          int maxLength,
          bool isFocused,
        }) =>
            null,
        style: TextStyle(
          fontFamily: 'Raleway',
        ),
        controller: _codeController,
        decoration: InputDecoration(hintText: 'Código'),
        textAlign: TextAlign.center,
        validator: (value) {
          if (value.isEmpty) {
            return 'Ingresé el código provisto';
          }
          return null;
        },
      ),
    );

    final loading = Container(
      height: 20,
      width: 20,
      child: CircularProgressIndicator(
        backgroundColor: ThemeColors.primaryLight,
        strokeWidth: 2,
      ),
    );

    final nextBtn = Container(
      margin: EdgeInsets.only(top: 10),
      child: RaisedButton(
        textColor: Theme.of(context).primaryColor,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(35.0),
        ),
        elevation: 0,
        highlightElevation: 0,
        disabledElevation: 0,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: true
            ? Text(
                'Siguiente',
                style: TextStyle(
                  fontFamily: 'Raleway',
                  fontWeight: FontWeight.w600,
                ),
              )
            : loading,
        onPressed: _validateInputs,
      ),
    );

    final backBtn = Container(
      margin: EdgeInsets.only(
        top: 8,
        bottom: 12,
      ),
      child: RaisedButton(
        color: Colors.transparent,
        textColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(35.0),
        ),
        elevation: 0,
        highlightElevation: 0,
        disabledElevation: 0,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Text(
          'Volver',
          style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w600),
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );

    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: Container(
        margin: EdgeInsets.only(top: 30),
        width: 250,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[inputCode, nextBtn, backBtn],
        ),
      ),
    );
  }
}
