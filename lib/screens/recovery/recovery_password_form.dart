import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/recoverypassword/recovery_password_bloc.dart';
import 'package:flutter_hemoterapia/blocs/recoverypassword/recovery_password_event.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';

class RecoveryNewPasswordView extends StatefulWidget {
  @override
  _RecoveryNewPasswordViewState createState() =>
      _RecoveryNewPasswordViewState();
}

class _RecoveryNewPasswordViewState extends State<RecoveryNewPasswordView> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _passwordController = TextEditingController();
  TextEditingController _repeatController = TextEditingController();
  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    _onSubmitButtonPressed() {
      BlocProvider.of<RecoveryPasswordBloc>(context).add(
          RecoveryPasswordButtonPressed(password: _passwordController.text));
    }

    void _validateInputs() {
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        _onSubmitButtonPressed();
      } else {
        setState(() {
          _autoValidate = true;
        });
      }
    }

    final inputPassword = Container(
      width: 250,
      child: TextFormField(
        controller: _passwordController,
        style: TextStyle(
          fontFamily: 'Raleway',
        ),
        obscureText: true,
        decoration: InputDecoration(hintText: 'Nueva contraseña'),
        textAlign: TextAlign.center,
        validator: (value) {
          if (value.isEmpty) {
            return 'Ingrese una contraseña';
          }
          if (value.length < 8) {
            return 'Debe tener al menos 8 caracteres';
          }
          return null;
        },
      ),
    );

    final inputRepeat = Container(
      width: 250,
      margin: EdgeInsets.only(top: 10),
      child: TextFormField(
        controller: _repeatController,
        style: TextStyle(
          fontFamily: 'Raleway',
        ),
        obscureText: true,
        decoration: InputDecoration(hintText: 'Repite contraseña'),
        textAlign: TextAlign.center,
        validator: (value) {
          if (value.isEmpty) {
            return 'Ingrese nuevamente la contraseña';
          }
          if (value != _passwordController.text) {
            return 'Las contraseñas no coinciden';
          }
          return null;
        },
      ),
    );

    final loading = Container(
      height: 20,
      width: 20,
      child: CircularProgressIndicator(
        backgroundColor: ThemeColors.primaryLight,
        strokeWidth: 2,
      ),
    );

    final nextText = Text(
      'Cambiar contraseña',
      style: TextStyle(
        fontFamily: 'Raleway',
        fontWeight: FontWeight.w600,
      ),
    );

    final btnNext = Container(
      margin: EdgeInsets.only(top: 10),
      child: RaisedButton(
        textColor: Theme.of(context).primaryColor,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(35.0),
        ),
        elevation: 0,
        highlightElevation: 0,
        disabledElevation: 0,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: true ? nextText : loading,
        onPressed: _validateInputs,
      ),
    );

    final btnBack = Container(
      margin: EdgeInsets.only(
        bottom: 10,
      ),
      child: RaisedButton(
        color: Colors.transparent,
        textColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(35.0),
        ),
        elevation: 0,
        highlightElevation: 0,
        disabledElevation: 0,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Text(
          'Volver',
          style: TextStyle(
            fontFamily: 'Raleway',
            fontWeight: FontWeight.w600,
          ),
        ),
        onPressed: () => Navigator.pushNamed(context, '/recovery_user'),
      ),
    );

    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: Container(
        margin: EdgeInsets.only(top: 30),
        width: 250,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[inputPassword, inputRepeat, btnNext, btnBack],
        ),
      ),
    );
  }
}
