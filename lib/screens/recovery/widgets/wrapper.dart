import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';

class RecoveryPasswordWrapper extends StatelessWidget {
  final Widget child;

  RecoveryPasswordWrapper({@required this.child});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: ThemeColors.primary,
      body: SingleChildScrollView(
        child: Container(
          width: size.width,
          margin: EdgeInsets.only(top: 140),
          child: child,
        ),
      ),
    );
  }
}
