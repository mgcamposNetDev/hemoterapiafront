import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/recoverypassword/recovery_password.dart';
import 'package:flutter_hemoterapia/screens/recovery/recovery_password_form.dart';
import 'package:flutter_hemoterapia/screens/recovery/widgets/header.dart';
import 'package:flutter_hemoterapia/screens/recovery/widgets/wrapper.dart';

class RecoveryNewPasswordScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RecoveryPasswordWrapper(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          HeaderLogoRecoveryPassword(
            subtitle: 'Nueva contraseña',
            description:
                'Ingrese su nueva contraseña y repitala para confirmar.',
          ),
          BlocProvider(
            create: (context) => RecoveryPasswordBloc(),
            child: BlocConsumer<RecoveryPasswordBloc, RecoveryPasswordState>(
              listener: (context, state) {
                if (state is RecoveryPasswordSuccess) {
                  BlocProvider.of<RecoveryPasswordBloc>(context)..add(RecoveryPasswordReset());
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/login', (Route<dynamic> route) => false);
                }
                if (state is RecoveryPasswordFailured) {
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Algo salió mal: ${state.error}'),
                    ),
                  );
                }
              },
              builder: (context, state) {
                return RecoveryNewPasswordView();
              },
            ),
          ),
        ],
      ),
    );
  }
}
