import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/models/building.dart';
import 'package:flutter_hemoterapia/screens/where_donate/widgets/map.dart';
import 'package:flutter_hemoterapia/utils/custom_font_icons.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class WhereToDonateScreen extends StatefulWidget {
  @override
  _WhereToDonateScreenState createState() => _WhereToDonateScreenState();
}

class _WhereToDonateScreenState extends State<WhereToDonateScreen> {
  Building selectedBuilding;
  bool isSelectedBuilding = false;
  LatLng position;

  final List<Building> listBuildings = [
    new Building(
      id: 1,
      address: "Av. Bandera de los Andes 2603, Guaymallén, Mendoza",
      name: "Hospital Notti",
      distance: 0.6,
      x: -32.8970495,
      y: -68.807516,
      hours: "Todos los días - 8:00 a 10:00",
      phone: "(0261) - 42613242",
    ),
    new Building(
      id: 2,
      address: "Av. de Acceso Este 1070, San José, Mendoza",
      name: "Hospital Italiano",
      distance: 0.6,
      x: -32.8978281,
      y: -68.8193317,
      hours: "Lun a Vie - 8:00 a 21:00",
      phone: "(0261) - 4261344",
    ),
    new Building(
      id: 3,
      address: "Av. Alem S/N, Mendoza",
      name: "Hospital Central",
      distance: 0.6,
      x: -32.8933572,
      y: -68.8311519,
      hours: "Todos los días - 8:00 a 12:00",
      phone: "(0261) - 42563487",
    ),
    new Building(
      id: 4,
      address: "Av. José Vicente Zapata 63, M5500 Mendoza",
      name: "Clínica de Cuyo S.A.",
      distance: 0.6,
      x: -32.8961674,
      y: -68.8392374,
      hours: "Lun a Vie - 8:00 a 17:00",
      phone: "(0261) - 4200000",
    )
  ];

  GoogleMapController mapController;

  final LatLng _center = const LatLng(-32.8998605, -68.8269314);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Container(
        width: size.width,
        height: size.height - 81,
        child: Column(
          children: <Widget>[
            mapWidget(context),
            Expanded(
              flex: 1,
              child: nearbyCareCenters(context),
            )
          ],
        ),
      ),
    );
  }

  Widget mapWidget(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.blue,
          width: size.width,
          height: size.width - 80,
          child: MapPage(
            pinPillopen: isSelectedBuilding,
            currentBuilding: selectedBuilding,
            buildings: listBuildings,
            center: _center,
            location: position,
          ),
        ),
        // Positioned(
        //   bottom: 12,
        //   right: 12,
        //   child: Container(
        //     width: 50,
        //     height: 50,
        //     child: FloatingActionButton(
        //       onPressed: () {},
        //       elevation: 4,
        //       backgroundColor: Colors.white,
        //       child: Icon(
        //         Icons.gps_fixed,
        //         size: 18,
        //       ),
        //       mini: true,
        //       highlightElevation: 3,
        //     ),
        //   ),
        // ),
      ],
    );
  }

  Widget nearbyCareCenters(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          titleNearbyCareCenters(context),
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 24),
              width: 300,
              child: ListView.separated(
                separatorBuilder: (BuildContext context, int index) {
                  return Divider(
                    height: 1,
                    color: Colors.white24,
                  );
                },
                itemCount: listBuildings.length,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () => setState(() {
                      isSelectedBuilding = true;
                      selectedBuilding = listBuildings[index];
                      position = LatLng(listBuildings[index].x, listBuildings[index].y);
                    }),
                    child: itemLocation(
                      listBuildings[index].name,
                      listBuildings[index].address,
                      listBuildings[index].distance,
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget titleNearbyCareCenters(BuildContext context) {
    return Container(
      color: ThemeColors.secondary,
      height: 40,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            CustomFont.gota,
            color: Colors.white,
            size: 14,
          ),
          SizedBox(width: 10),
          Text(
            "Centros más cercanos",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w600,
              fontFamily: 'Raleway',
            ),
          )
        ],
      ),
    );
  }

  Widget itemLocation(String building, String direction, double distance) {
    return Container(
      width: 300,
      height: 68,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 6),
                    child: Text(
                      building,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Raleway',
                      ),
                    ),
                  ),
                  Text(
                    direction,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontFamily: 'Raleway',
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: 90,
            child: Container(
              height: 30,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColorDark,
                borderRadius: BorderRadius.circular(4),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    CustomFont.gps,
                    color: Colors.white,
                    size: 14,
                  ),
                  SizedBox(width: 10),
                  Text(
                    distance.toString() + " km",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Raleway',
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
