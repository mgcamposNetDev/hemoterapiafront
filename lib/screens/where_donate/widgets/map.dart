import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_hemoterapia/models/building.dart';
import 'package:flutter_hemoterapia/screens/where_donate/widgets/map_pill.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';

class MapPage extends StatefulWidget {
  final List<Building> buildings;
  bool pinPillopen = false;
  final LatLng center;
  Building currentBuilding;
  LatLng location;

  MapPage(
      {@required this.buildings,
      @required this.center,
      this.currentBuilding,
      this.pinPillopen,
      this.location});
  @override
  State<StatefulWidget> createState() => MapPageState();
}

class MapPageState extends State<MapPage> {
  double CAMERA_ZOOM = 14.4;
  double CAMERA_TILT = 0;
  double CAMERA_BEARING = 30;
  bool showPinInfo = true;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  BitmapDescriptor pinIcon;
  LatLng currentLocation;
  GoogleMapController _mapController;

  @override
  void initState() {
    super.initState();
    currentLocation = widget.center ?? LatLng(42.7477863, -71.1699932);
    setPinIcon();
  }

  @override
  void didUpdateWidget(MapPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget != null && widget != null) {
      if (oldWidget.location?.latitude != widget.location?.latitude ||
          oldWidget.location?.longitude != widget.location?.longitude) {
        currentLocation = widget.location;
        _moveCamera(widget.location);
      }
    }
  }

  void setPinIcon() async {
    pinIcon = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(size: Size(48, 48)),
      'assets/images/pin.png',
    );
  }

  void _moveCamera(LatLng position) {
    _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: position, zoom: CAMERA_ZOOM),
      ),
    );
  }

  void onMapCreated(GoogleMapController controller) {
    _mapController = controller;
    setMapPins();
  }

  void setMapPins() {
    Map<MarkerId, Marker> markersNew = <MarkerId, Marker>{};
    for (var build in widget.buildings) {
      MarkerId markerId = MarkerId(build.id.toString());
      LatLng location = LatLng(build.x, build.y);
      // creating a new MARKER
      final Marker marker = Marker(
        markerId: markerId,
        position: location,
        icon: pinIcon,
        onTap: () => setState(() {
          widget.pinPillopen = true;
          widget.currentBuilding = build;
        }),
      );

      markersNew[markerId] = marker;
    }

    setState(() {
      // adding a new marker to map
      markers = markersNew;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          GoogleMap(
            gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
              new Factory<OneSequenceGestureRecognizer>(
                () => new EagerGestureRecognizer(),
              ),
            ].toSet(),
            markers: Set<Marker>.of(markers.values),
            mapType: MapType.normal,
            initialCameraPosition: CameraPosition(
              zoom: CAMERA_ZOOM,
              bearing: CAMERA_BEARING,
              tilt: CAMERA_TILT,
              target: currentLocation,
            ),
            myLocationEnabled: true,
            onMapCreated: onMapCreated,
            mapToolbarEnabled: false,
            onTap: (LatLng location) {
              setState(() {
                widget.pinPillopen = false;
              });
            },
          ),
          MapPinPill(
            currentlyBuilding: widget.currentBuilding,
            pinPillPosition: widget.pinPillopen ? 0 : -100,
          )
        ],
      ),
    );
  }
}
