import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/models/building.dart';
import 'package:flutter_hemoterapia/screens/where_donate/widgets/map_pill_info.dart';
import 'package:flutter_hemoterapia/screens/where_donate/widgets/circle_logo.dart';
import 'package:flutter_hemoterapia/screens/where_donate/widgets/map_info.dart';

class MapPinPill extends StatelessWidget {
  final double pinPillPosition;
  final Building currentlyBuilding;

  MapPinPill(
      {Key key,
      @required this.currentlyBuilding,
      @required this.pinPillPosition})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      child: MapPillWrapper(
        leading: CircleIconLogo(),
        content: currentlyBuilding != null
            ? HospitalInfo(
                buildingInfo: currentlyBuilding,
              )
            : Container(),
      ),
      duration: Duration(milliseconds: 200),
      bottom: pinPillPosition,
    );
  }
}
