import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/authentication/authentication_bloc.dart';
import 'package:flutter_hemoterapia/blocs/authentication/authentication_state.dart';
import 'package:flutter_hemoterapia/screens/widgets/progress.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
  }

  Widget get _logoView {
    return Container(
      child: Column(
        children: <Widget>[
          LogoAnimated(),
          SizedBox(height: 12),
          Text(
            'CENTRO REGIONAL',
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            'DE HEMOTERAPIA',
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (context, state) {
        if (state is AuthenticationUnauthenticated) {
          print("no authenticated");
          Navigator.of(context).pushNamed('/login');
        }
        if (state is AuthenticationAuthenticated) {
          print("authenticate");
          Navigator.of(context).pushNamed('/home');
        }
        if (state is AuthenticationAuthenticatedFirstTime) {
          print("authenticate first time");
          Navigator.of(context).pushNamed('/walkthrough');
        }
      },
      child: Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _logoView,
              SizedBox(height: 12),
            ],
          ),
        ),
      ),
    );
  }
}
