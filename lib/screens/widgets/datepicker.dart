import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/themes/theme.dart';
import 'package:intl/intl.dart';

class DatePicker extends StatefulWidget {
  final TextStyle style;
  final TextAlign textAlign;
  final String initialValue;
  final TextEditingController controller;
  final InputDecoration decoration;
  final String Function(String) validator;
  final DateTime firstDate;
  final DateTime lastDate;
  final Function(String) onSaved;
  DatePicker(
      {this.style,
      this.textAlign,
      this.initialValue,
      this.controller,
      this.decoration,
      this.validator,
      this.firstDate,
      this.lastDate,
      this.onSaved});

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  DateTime selectedDate;
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.initialValue != null) {
      DateTime parsed = DateFormat("dd-MM-yyyy").parse(widget.initialValue);
      _controller.text = new DateFormat('dd/MM/yyyy').format(parsed);
      selectedDate = parsed;
    }
  }

  DateTime _setInitialDate() {
    if (selectedDate == null && widget.initialValue != null) {
      return new DateFormat("dd-MM-yyyy").parse(widget.initialValue);
    } else if (selectedDate == null && widget.initialValue == null) {
      return DateTime.now();
    } else {
      return selectedDate;
    }
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _setInitialDate(),
        firstDate: DateTime(1900),
        lastDate: DateTime.now(),
        builder: (BuildContext context, Widget widget) {
          return Theme(data: getDatePickerTheme(), child: widget);
        });
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        if (widget.controller != null) {
          widget.controller.text = new DateFormat('dd/MM/yyyy').format(picked);
        } else {
          _controller.text = new DateFormat('dd/MM/yyyy').format(picked);
        }
      });
  }

  void _onSaved() {
    widget.onSaved(new DateFormat('dd-MM-yyyy').format(selectedDate));
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      style: widget.style,
      textAlign: widget.textAlign,
      controller: widget.controller ?? _controller,
      readOnly: true,
      onTap: () => _selectDate(context),
      decoration: widget.decoration,
      validator: widget.validator,
      onSaved: (value) => _onSaved(),
    );
  }
}
