import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';
import 'package:flutter_svg/svg.dart';

class ProgressLogo extends StatelessWidget {
  const ProgressLogo();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeColors.white,
      child: Center(
        child: LogoAnimated(),
      ),
    );
  }
}

class LogoAnimated extends StatefulWidget {
  @override
  _LogoAnimatedState createState() => _LogoAnimatedState();
}

class _LogoAnimatedState extends State<LogoAnimated>
    with TickerProviderStateMixin {
  AnimationController rotationController;
  AnimationController fadeController;
  Animation fadeAnimation;

  @override
  void initState() {
    super.initState();
    rotationController = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    fadeController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    fadeAnimation = Tween(begin: 0.0, end: 1.0).animate(fadeController);
    rotationController.repeat();
    fadeController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: fadeAnimation,
      child: Container(
        width: 54,
        height: 54,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(37),
          color: ThemeColors.primary,
        ),
        child: new AnimatedBuilder(
          animation: rotationController,
          child: Container(
            child: new SvgPicture.asset(
              'assets/images/logo.svg',
              semanticsLabel: 'logo',
            ),
          ),
          builder: (BuildContext context, Widget widget) {
            return new Transform.rotate(
              angle: rotationController.value * 6.3,
              child: widget,
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    rotationController.dispose();
    super.dispose();
  }
}
