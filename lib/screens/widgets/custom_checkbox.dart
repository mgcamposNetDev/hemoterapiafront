import 'package:flutter/material.dart';

class CustomCheckbox extends StatefulWidget {
  final String label;
  final bool value;
  final Function(bool) onChange;
  final Color checkColor, activeColor;
  
  CustomCheckbox({ Key key, @required this.onChange, this.label, this.value, this.checkColor, this.activeColor })
    : super(key: key);

  @override
  _CheckboxState createState() => _CheckboxState();
}

class _CheckboxState extends State<CustomCheckbox> {
  _onChange(bool value) {
    widget.onChange(value);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Checkbox(
            value: widget.value ?? false,
            onChanged: _onChange,
            checkColor: widget.checkColor ?? Theme.of(context).primaryColor,
            activeColor: widget.activeColor ?? Colors.white,
          ),
          Text(widget.label ?? ""),
        ],
      ),
    );
  }
}
