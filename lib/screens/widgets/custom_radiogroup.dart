import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

enum CustomRadioOrientation { horizontal, vertical }

class CustomRadioGroup extends StatefulWidget {
  final List<RadioItem> items;
  final String title;
  final CustomRadioOrientation orientation;
  final RadioController controller;
  final TextStyle style;
  final Color color;
  CustomRadioGroup({
    Key key,
    @required this.items,
    this.title,
    this.orientation,
    this.controller,
    this.style,
    this.color
  })  : assert(items != null && items.length > 0),
        super(key: key);


  @override
  _CustomRadioGroupState createState() => _CustomRadioGroupState();
}

class _CustomRadioGroupState extends State<CustomRadioGroup> {
  _handleItemSelected(dynamic value) {
    setState(() {
      widget.controller.value = value;
    });
  }

  @override
  void initState() {
    super.initState();
    if (widget.items != null && widget.items.length > 0) {
      widget.controller.value = widget.items[0].value;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Sexo',
            style: widget.style,
          ),
          widget.orientation == CustomRadioOrientation.horizontal
              ? new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: buildRadioItem(),
                )
              : new Column(
                  children: buildRadioItem(),
                ),
        ],
      ),
    );
  }

  List<Widget> buildRadioItem() {
    return widget.items.map(
      (item) => Row(
        children: <Widget>[
          new Radio(
            value: item.value,
            groupValue: widget.controller.value,
            onChanged: _handleItemSelected,
            activeColor: widget.color,
            focusColor: widget.color,
            hoverColor: widget.color,
          ),
          new Text(
            item.label,
            style: widget.style,
          ),
        ],
      ),
    ).toList();
  }
}

class RadioItem {
  String label;
  dynamic value;
  RadioItem({@required this.label, @required this.value})
      : assert(label != null, value != null);
}

class RadioController {
  dynamic _value;

  RadioController({dynamic value}) {
    this._value = value;
  }

  dynamic get value => this._value;

  set value(dynamic value) {
    _value = value;
  }
}
