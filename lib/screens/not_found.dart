import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

class NotFoundScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: NotFoundBody(),
    );
  }
}

class NotFoundBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Stack(
      children: <Widget>[
        Positioned(
          top: -(width * 2 / 7),
          width: width,
          height: height,
          child: backgroundDroplet(context),
        ),
        Positioned(
          top: height * 3 / 7 - 20,
          width: width,
          child: Center(
            child: _logoView,
          ),
        )
      ],
    );
  }

  Widget backgroundDroplet(BuildContext context) {
    final String assetName = 'assets/images/fondo-gota.svg';
    final Widget svgImage = new SvgPicture.asset(
      assetName,
      color: Theme.of(context).primaryColorLight,
      semanticsLabel: 'A red up arrow',
      fit: BoxFit.fitWidth,
    );
    double width = MediaQuery.of(context).size.width * 2;
    double height = MediaQuery.of(context).size.height * 2;

    return Container(
      width: width,
      height: height,
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Center(
            child: SizedBox(
              width: width,
              height: height,
              child: svgImage,
            ),
          )
        ],
      ),
    );
  }

  Widget get _logoView {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
              height: 30,
              width: 180,
              child: new SvgPicture.asset('assets/images/logo.svg',
                  semanticsLabel: 'logo')),
          SizedBox(height: 6),
          Text(
            'CENTRO REGIONAL',
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 12,
                letterSpacing: -0.5,
                fontWeight: FontWeight.w600),
          ),
          Text(
            'DE HEMOTERAPIA',
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 12,
                letterSpacing: -0.5,
                fontWeight: FontWeight.w600),
          ),
        ],
      ),
    );
  }
}
