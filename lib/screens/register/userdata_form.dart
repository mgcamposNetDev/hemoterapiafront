import 'dart:io';

import 'package:camera_camera/camera_camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/register/register_bloc.dart';
import 'package:flutter_hemoterapia/blocs/register/register_event.dart';
import 'package:flutter_hemoterapia/blocs/register/register_state.dart';
import 'package:flutter_hemoterapia/models/person.dart';
import 'package:flutter_hemoterapia/screens/widgets/circle_picture.dart';
import 'package:flutter_hemoterapia/screens/widgets/custom_checkbox.dart';
import 'package:flutter_hemoterapia/screens/widgets/custom_radiogroup.dart';
import 'package:flutter_hemoterapia/screens/widgets/datepicker.dart';
import 'package:flutter_svg/flutter_svg.dart';

class UserdataForm extends StatefulWidget {
  const UserdataForm({
    Key key,
  }) : super(key: key);

  @override
  _UserdataFormState createState() => _UserdataFormState();
}

class _UserdataFormState extends State<UserdataForm> {
  final _formKey = GlobalKey<FormState>();

  List _bloodTypes = ["A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-"];
  List _docTypes = ["DNI", "Pasaporte", "CI", "LE", "LC"];
  List _countries = ["Argentina"];
  List _provinces = ["Mendoza"];
  List _departments = ["Guaymallén"];

  List<RadioItem> _sexItems = [
    RadioItem(label: "Femenino", value: 'f'),
    RadioItem(label: "Masculino", value: 'm'),
  ];

  String _dateBirth;
  String _pathPicture = '';
  final _sexController = RadioController();
  String _bloodTypeSelected;
  String _documentTypeSelected;
  String _countrySelected;
  String _provinceSelected;
  String _departmentSelected;
  final _dateBirthController = TextEditingController();
  final _nameController = TextEditingController();
  final _surnameController = TextEditingController();
  final _documentController = TextEditingController();
  final _phoneController = TextEditingController();
  final _addressController = TextEditingController();
  final _postalcodeController = TextEditingController();
  final _weightController = TextEditingController();
  bool _prevDonation = false;
  bool _autoValidate = false;

  DateTime selectedDate;

  @override
  void initState() {
    selectedDate = new DateTime.now();
    super.initState();
  }

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _onRegisterButtonPressed();
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  void _handlePreviousDonation(bool value) {
    setState(() {
      _prevDonation = value;
    });
  }

  _onRegisterButtonPressed() {
    final person = new Person(
      name: _nameController.text,
      surname: _surnameController.text,
      bloodType: _bloodTypeSelected,
      dateBirth: _dateBirthController.text,
      document: _documentController.text,
      documentType: _documentTypeSelected,
      phone: _phoneController.text,
      sex: _sexController.value,
      weigth: _weightController.text,
    );

    BlocProvider.of<RegisterBloc>(context).add(
      UserdataButtonPressed(person: person),
    );
  }

  Future<void> _onAddPhoto() async {
    File file = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Camera(
          orientationEnablePhoto: CameraOrientation.portrait,
          imageMask: CameraFocus.circle(
            color: Colors.black.withOpacity(0.5),
          ),
          mode: CameraMode.normal,
        ),
      ),
    );

    setState(() {
      _pathPicture = file.uri.toFilePath();
    });
  }

  List<DropdownMenuItem> get _getBloodTypeMenuItem {
    List<DropdownMenuItem> items = new List();
    for (String bloodType in _bloodTypes) {
      items.add(
        new DropdownMenuItem(
          value: bloodType,
          child: Container(
            margin: EdgeInsets.only(
              left: 10,
            ),
            child: new Text(
              "Grupo " + bloodType,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontFamily: 'Raleway'),
            ),
          ),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem> get _getDocTypeMenuItem {
    List<DropdownMenuItem> items = new List();
    for (String docType in _docTypes) {
      items.add(
        DropdownMenuItem(
          value: docType,
          child: Container(
            margin: EdgeInsets.only(
              left: 10,
            ),
            child: new Text(
              docType,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontFamily: 'Raleway'),
            ),
          ),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem> get _getCountryMenuItem {
    List<DropdownMenuItem> items = new List();
    for (String country in _countries) {
      items.add(
        DropdownMenuItem(
          value: country,
          child: Container(
            margin: EdgeInsets.only(
              left: 10,
            ),
            child: new Text(
              country,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontFamily: 'Raleway'),
            ),
          ),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem> get _getProvinceMenuItem {
    List<DropdownMenuItem> items = new List();
    for (String province in _provinces) {
      items.add(
        new DropdownMenuItem(
          value: province,
          child: Container(
            margin: EdgeInsets.only(
              left: 10,
            ),
            child: new Text(
              province,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontFamily: 'Raleway'),
            ),
          ),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem> get _getDepartmentMenuItem {
    List<DropdownMenuItem> items = new List();
    for (String department in _departments) {
      items.add(
        new DropdownMenuItem(
          value: department,
          child: Container(
            margin: EdgeInsets.only(
              left: 10,
            ),
            child: new Text(
              department,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontFamily: 'Raleway'),
            ),
          ),
        ),
      );
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    final firstnameInput = Container(
      margin: EdgeInsets.only(top: 20),
      child: TextFormField(
        style: TextStyle(fontFamily: 'Raleway'),
        decoration: InputDecoration(hintText: 'Nombre'),
        textAlign: TextAlign.center,
        controller: _nameController,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Ingrese su nombre completo';
          }
          if (value == null || value.length > 40) {
            return 'Hasta 40 caractéres';
          }
          return null;
        },
      ),
    );

    final lastnameInput = Container(
      margin: EdgeInsets.only(top: 16),
      child: TextFormField(
        style: TextStyle(fontFamily: 'Raleway'),
        textAlign: TextAlign.center,
        decoration: InputDecoration(hintText: 'Apellido'),
        controller: _surnameController,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Ingrese su apellido completo';
          }
          if (value == null || value.length > 40) {
            return 'Hasta 40 caractéres';
          }
          return null;
        },
      ),
    );

    final documentSelect = Container(
      margin: EdgeInsets.only(top: 16),
      child: DropdownButtonFormField<dynamic>(
        value: _documentTypeSelected,
        items: _getDocTypeMenuItem,
        selectedItemBuilder: (BuildContext context) {
          return _getDocTypeMenuItem.map<Widget>((Widget item) {
            return Container(
              width: 200,
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[item],
              ),
            );
          }).toList();
        },
        decoration: InputDecoration(
          hintText: 'Tipo de documento',
          contentPadding:
              EdgeInsets.only(left: 20, right: 6, top: 8, bottom: 7),
        ),
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.white,
        ),
        onChanged: (value) => setState(() => _documentTypeSelected = value),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Seleccione un tipo de documento';
          }
          return null;
        },
      ),
    );

    final documentInput = Container(
      margin: EdgeInsets.only(top: 16),
      child: TextFormField(
        style: TextStyle(fontFamily: 'Raleway'),
        textAlign: TextAlign.center,
        decoration: InputDecoration(hintText: 'Numero de documento'),
        controller: _documentController,
        keyboardType: _documentTypeSelected == "Pasaporte"
            ? TextInputType.text
            : TextInputType.number,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Ingrese un número de documento';
          }
          if (value == null || value.length > 40) {
            return 'Hasta 40 caractéres';
          }
          return null;
        },
      ),
    );

    final birthdayPicker = Container(
      margin: EdgeInsets.only(top: 16),
      child: DatePicker(
        style: TextStyle(fontFamily: 'Raleway'),
        textAlign: TextAlign.center,
        initialValue: null,
        decoration: InputDecoration(hintText: 'Fecha de nacimiento'),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Ingrese su fecha de nacimiento';
          }
          return null;
        },
        onSaved: (value) => _dateBirth = value,
      ),
    );

    final phoneInput = Container(
      margin: EdgeInsets.only(top: 16),
      child: TextFormField(
        style: TextStyle(fontFamily: 'Raleway'),
        textAlign: TextAlign.center,
        decoration: InputDecoration(hintText: 'Teléfono'),
        controller: _phoneController,
        keyboardType: TextInputType.phone,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Ingrese su número de teléfono';
          }
          if (value == null || value.length > 40) {
            return 'Hasta 40 caractéres';
          }
          return null;
        },
      ),
    );

    final addressInput = Container(
      margin: EdgeInsets.only(top: 16),
      child: TextFormField(
        style: TextStyle(fontFamily: 'Raleway'),
        textAlign: TextAlign.center,
        decoration: InputDecoration(hintText: 'Domicilio'),
        controller: _addressController,
        keyboardType: TextInputType.text,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Ingrese su número de teléfono';
          }
          if (value == null || value.length > 200) {
            return 'Hasta 200 caractéres';
          }
          return null;
        },
      ),
    );

    final sexRadio = Container(
      margin: EdgeInsets.only(top: 10),
      child: CustomRadioGroup(
        items: _sexItems,
        controller: _sexController,
        orientation: CustomRadioOrientation.horizontal,
        title: "Sexo",
      ),
    );

    final weightInput = Container(
      margin: EdgeInsets.only(bottom: 16),
      child: TextFormField(
        style: TextStyle(fontFamily: 'Raleway'),
        textAlign: TextAlign.center,
        decoration: InputDecoration(hintText: 'Peso'),
        controller: _weightController,
        keyboardType: TextInputType.number,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Ingrese su peso';
          }
          if (value == null || value.length > 4) {
            return 'Hasta 4 caractéres';
          }
          return null;
        },
      ),
    );

    final countrySelect = Container(
      margin: EdgeInsets.only(top: 16),
      child: DropdownButtonFormField<dynamic>(
        value: _countrySelected,
        items: _getCountryMenuItem,
        selectedItemBuilder: (BuildContext context) {
          return _getCountryMenuItem.map<Widget>((Widget item) {
            return Container(
              width: 200,
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[item],
              ),
            );
          }).toList();
        },
        decoration: InputDecoration(
          hintText: 'País',
          contentPadding:
              EdgeInsets.only(left: 20, right: 6, top: 8, bottom: 7),
        ),
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.white,
        ),
        onChanged: (value) => setState(() => _countrySelected = value),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Seleccione su país';
          }
          return null;
        },
      ),
    );

    final provinceSelect = Container(
      margin: EdgeInsets.only(top: 16),
      child: DropdownButtonFormField<dynamic>(
        value: _provinceSelected,
        items: _getProvinceMenuItem,
        selectedItemBuilder: (BuildContext context) {
          return _getProvinceMenuItem.map<Widget>((Widget item) {
            return Container(
              width: 200,
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[item],
              ),
            );
          }).toList();
        },
        decoration: InputDecoration(
          hintText: 'Provincia',
          contentPadding:
              EdgeInsets.only(left: 20, right: 6, top: 8, bottom: 7),
        ),
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.white,
        ),
        onChanged: (value) => setState(() => _provinceSelected = value),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Seleccione su provincia';
          }
          return null;
        },
      ),
    );

    final departmentSelect = Container(
      margin: EdgeInsets.only(top: 16),
      child: DropdownButtonFormField<dynamic>(
        value: _departmentSelected,
        items: _getDepartmentMenuItem,
        selectedItemBuilder: (BuildContext context) {
          return _getDepartmentMenuItem.map<Widget>((Widget item) {
            return Container(
              width: 200,
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[item],
              ),
            );
          }).toList();
        },
        decoration: InputDecoration(
          hintText: 'Departamento',
          contentPadding:
              EdgeInsets.only(left: 20, right: 6, top: 8, bottom: 7),
        ),
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.white,
        ),
        onChanged: (value) => setState(() => _departmentSelected = value),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Seleccione su departamento';
          }
          return null;
        },
      ),
    );

    final postalCodeInput = Container(
      margin: EdgeInsets.only(top: 16),
      child: TextFormField(
        style: TextStyle(fontFamily: 'Raleway'),
        textAlign: TextAlign.center,
        decoration: InputDecoration(hintText: 'Código postal'),
        controller: _postalcodeController,
        keyboardType: TextInputType.phone,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Ingrese su código postal';
          }
          if (value == null || value.length > 4) {
            return 'Hasta 4 caractéres';
          }
          return null;
        },
      ),
    );

    final bloodTypeSelect = Container(
      margin: EdgeInsets.only(top: 16),
      child: DropdownButtonFormField<dynamic>(
        value: _bloodTypeSelected,
        items: _getBloodTypeMenuItem,
        selectedItemBuilder: (BuildContext context) {
          return _getBloodTypeMenuItem.map<Widget>((Widget item) {
            return Container(
              width: 200,
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[item],
              ),
            );
          }).toList();
        },
        decoration: InputDecoration(
          hintText: 'Grupo sanguíneo',
          contentPadding:
              EdgeInsets.only(left: 20, right: 6, top: 8, bottom: 7),
        ),
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.white,
        ),
        onChanged: (value) => setState(() => _bloodTypeSelected = value),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Seleccione su tipo de sangre';
          }
          return null;
        },
      ),
    );

    final previousDonation = Container(
      margin: EdgeInsets.only(top: 10),
      child: CustomCheckbox(
        value: _prevDonation,
        label: "¿Ha donado previamente?",
        onChange: _handlePreviousDonation,
      ),
    );

    final registerButton = Container(
      child: RaisedButton(
        textColor: Theme.of(context).primaryColor,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(35.0),
        ),
        elevation: 0,
        highlightElevation: 0,
        disabledElevation: 0,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Text(
          'Registrarse',
          style: TextStyle(
            fontFamily: 'Raleway',
            fontWeight: FontWeight.w600,
          ),
        ),
        onPressed: _validateInputs,
      ),
    );

    final backButton = Container(
      margin: EdgeInsets.only(top: 8),
      child: RaisedButton(
        color: Colors.transparent,
        textColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(35.0),
        ),
        elevation: 0,
        highlightElevation: 0,
        disabledElevation: 0,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Text(
          'Volver',
          style: TextStyle(
            fontFamily: 'Raleway',
            fontWeight: FontWeight.w600,
          ),
        ),
        onPressed: () => Navigator.pop(context),
      ),
    );

    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state is RegisterFailure) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text('${state.error}')),
          );
        }
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        builder: (context, state) {
          return Form(
            autovalidate: _autoValidate,
            key: _formKey,
            child: Container(
              width: 250,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _pathPicture.isEmpty
                      ? Container(
                          height: 80,
                          width: 250,
                          child: InkWell(
                            onTap: _onAddPhoto,
                            child: new SvgPicture.asset(
                              'assets/images/add-photo.svg',
                              semanticsLabel: 'add photo',
                            ),
                          ),
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CirclePicture(
                              path: _pathPicture,
                              isUrl: false,
                              size: 80,
                            ),
                          ],
                        ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    width: 250,
                    child: Text(
                      'Sube una imagen de perfil',
                      style: TextStyle(fontFamily: 'Raleway'),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  firstnameInput,
                  lastnameInput,
                  documentSelect,
                  documentInput,
                  birthdayPicker,
                  phoneInput,
                  countrySelect,
                  provinceSelect,
                  departmentSelect,
                  addressInput,
                  postalCodeInput,
                  sexRadio,
                  weightInput,
                  Container(
                    width: 250,
                    child: Text(
                      'Grupo sanguíneo',
                      style: TextStyle(fontFamily: 'Raleway'),
                    ),
                  ),
                  bloodTypeSelect,
                  previousDonation,
                  registerButton,
                  backButton,
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
