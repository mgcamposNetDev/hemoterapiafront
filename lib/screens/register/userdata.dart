import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/register/register_bloc.dart';
import 'package:flutter_hemoterapia/blocs/signup/signup_bloc.dart';
import 'package:flutter_hemoterapia/blocs/signup/signup_state.dart';
import 'package:flutter_hemoterapia/repository/user_repository.dart';
import 'package:flutter_hemoterapia/screens/register/userdata_form.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RegisterUserDataScreen extends StatelessWidget {
  final UserRepository userRepository;

  RegisterUserDataScreen({Key key, @required this.userRepository});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: BlocProvider(
        create: (context) {
          return new RegisterBloc(
            userRepository: userRepository,
            signupBloc: BlocProvider.of<SignupBloc>(context),
          );
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 140, bottom: 30),
            child: Center(
              child: RegisterUserDataView(),
            ),
          ),
        ),
      ),
    );
  }
}

class RegisterUserDataView extends StatefulWidget {
  RegisterUserDataView({Key key}) : super(key: key);

  @override
  _RegisterUserDataState createState() => new _RegisterUserDataState();
}

class _RegisterUserDataState extends State<RegisterUserDataView> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<SignupBloc, SignupState>(
      listener: (context, state) {
        if (state is SignupUserdataEntered) {
          Navigator.of(context).pushNamed('/walkthrough');
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _logoView,
          _description,
          SizedBox(height: 30),
          UserdataForm()
        ],
      ),
    );
  }

  Widget get _logoView {
    return SizedBox(
      width: 250,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50,
            child: new SvgPicture.asset(
              'assets/images/logo.svg',
              semanticsLabel: 'logo',
            ),
          ),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget get _description {
    return SizedBox(
      width: 250,
      child: Column(
        children: <Widget>[
          Text(
            'Datos personales',
            style: TextStyle(
              fontFamily: 'Raleway',
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: 20),
          Text(
            'Completa los siguientes campos para completar la registración',
            style: TextStyle(fontFamily: 'Raleway'),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}
