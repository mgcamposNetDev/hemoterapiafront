import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class RecoveryPasswordState extends Equatable {  
  @override
  List<Object> get props => [];
}

class RecoveryPasswordUninitialized extends RecoveryPasswordState {
  @override
  String toString() => 'RecoveryPasswordUninitialized';
}

class RecoveryPasswordLoading extends RecoveryPasswordState {
  @override
  String toString() => 'RecoveryPasswordLoading';
}

class RecoveryPasswordInitialized extends RecoveryPasswordState {
  @override
  String toString() => 'RecoveryPasswordInitialized';
}

class RecoveryPasswordFailured extends RecoveryPasswordState {
  final String error;

  RecoveryPasswordFailured(this.error);

  @override
  String toString() => 'RecoveryPasswordFailured ${this.error}';
}

class RecoveryPasswordSuccess extends RecoveryPasswordState {
  @override
  String toString() => 'RecoveryPasswordSuccess';
}
