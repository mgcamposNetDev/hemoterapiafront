import 'package:bloc/bloc.dart';
import 'package:flutter_hemoterapia/blocs/recoverypassword/recovery_password_event.dart';
import 'package:flutter_hemoterapia/blocs/recoverypassword/recovery_password_state.dart';

class RecoveryPasswordBloc
    extends Bloc<RecoveryPasswordEvent, RecoveryPasswordState> {
  @override
  RecoveryPasswordState get initialState => RecoveryPasswordUninitialized();

  @override
  Stream<RecoveryPasswordState> mapEventToState(
    RecoveryPasswordEvent event,
  ) async* {
    if (event is RecoveryPasswordButtonPressed) {
      yield* _mapRecoveryPasswordButtonPressedToState(event);
    }
    if (event is RecoveryPasswordReset) {}
  }

  Stream<RecoveryPasswordState> _mapRecoveryPasswordButtonPressedToState(
      RecoveryPasswordButtonPressed event) async* {
    try {
      yield RecoveryPasswordSuccess();
    } catch (error) {
      yield RecoveryPasswordFailured(error);
    }
  }

  Stream<RecoveryPasswordState> _mapRecoveryPasswordResetToState(
      RecoveryPasswordReset event) async* {
    yield RecoveryPasswordUninitialized();
  }
}
