import 'package:equatable/equatable.dart';

abstract class SignupState extends Equatable {
  const SignupState();
  @override
  List<Object> get props => [];
}
// inicializa el registro
class SignupUnitialized extends SignupState {}

// registrado el usuario 
class SignupUserRegistered extends SignupState {}

// se agrego la data personal del usuario
class SignupUserdataEntered extends SignupState {}

// el formulario se está guardando
class SignupLoading extends SignupState {}