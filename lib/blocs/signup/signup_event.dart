import 'package:equatable/equatable.dart';
import 'package:flutter_hemoterapia/models/person.dart';
import 'package:meta/meta.dart';

abstract class SignupEvent extends Equatable {
  const SignupEvent();

  @override
  List<Object> get props => [];
}

// el registro fue realizado y se recibió el token del usuario
class UserRegistered extends SignupEvent {
  final String token;

  const UserRegistered({@required this.token});

  @override
  List<Object> get props => [token];

  @override
  String toString() => 'UserRegistered { token: $token }';
}

// el usuario ingreso sus datos personales
class UserdataEntered extends SignupEvent {
  final Person person;

  const UserdataEntered({this.person});

  @override
  List<Object> get props => [person];

  @override
  String toString() => 'UserdataEntered';
}
