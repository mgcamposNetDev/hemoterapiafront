import 'package:equatable/equatable.dart';
import 'package:flutter_hemoterapia/models/person.dart';

class SettingsState extends Equatable {
  const SettingsState();

  @override
  List<Object> get props => [];
}

class SettingsUninitialized extends SettingsState {
  @override
  String toString() => 'SettingsUninitialized';
}

class SettingsInitilized extends SettingsState {
  final Person personalData;

  SettingsInitilized(this.personalData);

  @override
  String toString() => 'SettingsInitilized';
}

class SettingsLoading extends SettingsState {
  @override
  String toString() => 'SettingsLoading';
}

class SettingsSaving extends SettingsState {
  @override
  String toString() => 'SettingsLoading';
}

