import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class SettingsEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class SettingsFetch extends SettingsEvent {
  @override
  String toString() => "SettingsFetch";
}

class SettingsSave extends SettingsEvent {
  final String name;
  final String surname;
  final String document;
  final String dateBirth;
  final String sex;
  final String weigth;
  final String profileUrl;
  final String bloodType;

  SettingsSave({
    @required this.name,
    @required this.surname,
    @required this.document,
    @required this.dateBirth,
    @required this.sex,
    @required this.weigth,
    @required this.profileUrl,
    @required this.bloodType,
  });

  @override
  String toString() => "SettingsFetch";
}
