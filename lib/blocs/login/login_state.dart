import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}

// Is the initial state of the LoginForm
class LoginInitial extends LoginState {}

// Is the state of the LoginForm when we are validating credentials
class LoginLoading extends LoginState {}

// Is the state of the LoginForm when a login attempt has failed.
class LoginFailure extends LoginState {
  final String error;

  const LoginFailure({@required this.error}) : assert(error != null);

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure {error: $error}';

}