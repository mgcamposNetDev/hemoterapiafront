
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class RecoveryCodeEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RecoveryCodeButtonPressed extends RecoveryCodeEvent {
  final String code;

  RecoveryCodeButtonPressed({ @required this.code });

  @override
  String toString() => 'RecoveryCodeButtonPressed';
}


class RecoveryCodeReset extends RecoveryCodeEvent {
  RecoveryCodeReset();

  @override
  String toString() => 'RecoveryCodeReset';
}

