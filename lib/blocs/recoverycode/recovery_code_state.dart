import 'package:equatable/equatable.dart';

class RecoveryCodeState extends Equatable {
  @override
  List<Object> get props => [];
}

class RecoveryCodeUninitialized extends RecoveryCodeState {
  @override
  String toString() => 'RecoveryCodeUninitialized';
}

class RecoveryCodeLoading extends RecoveryCodeState {
  @override
  String toString() => 'RecoveryCodeLoading';
}

class RecoveryCodeInitialized extends RecoveryCodeState {
  @override
  String toString() => 'RecoveryCodeInitialized';
}

class RecoveryCodeFailured extends RecoveryCodeState {
  final String error;

  RecoveryCodeFailured(this.error);

  @override
  String toString() => 'RecoveryCodeFailured ${this.error}';
}

class RecoveryCodeSuccess extends RecoveryCodeState {
  RecoveryCodeSuccess();

  @override
  String toString() => 'RecoveryCodeSuccess';
}