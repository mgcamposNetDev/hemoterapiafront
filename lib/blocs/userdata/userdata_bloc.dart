import 'package:bloc/bloc.dart';
import 'package:flutter_hemoterapia/blocs/userdata/userdata_event.dart';
import 'package:flutter_hemoterapia/blocs/userdata/userdata_state.dart';

class UserDataBloc extends Bloc<UserDataEvent, UserDataState> {
  @override
  UserDataState get initialState => UserDataUninitialzed();

  @override
  Stream<UserDataState> mapEventToState(
    UserDataEvent event,
  ) async* {
    if (event is UserDataInit) {
      yield UserDataInitialized(event.getPerson());
    }
  }
}