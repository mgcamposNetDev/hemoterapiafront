import 'package:equatable/equatable.dart';
import 'package:flutter_hemoterapia/models/person.dart';

class UserDataEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class UserDataInit extends UserDataEvent {
  final Person _person;
  UserDataInit(Person person): _person = person;

  Person getPerson() => _person;
  String toString() => 'UserDataInit';
}