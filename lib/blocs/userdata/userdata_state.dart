import 'package:equatable/equatable.dart';
import 'package:flutter_hemoterapia/models/person.dart';

class UserDataState extends Equatable{
  UserDataState();
  @override
  List<Object> get props => [];
}

class UserDataUninitialzed extends UserDataState {
  @override
  String toString() => "UserDataUninitialzed";
}

class UserDataInitialized extends UserDataState {
  final Person _person;

  UserDataInitialized(Person person): this._person = person;

  Person getPerson() => _person;

  @override
  String toString() => "UserDataInitialized";
}

class UserDataLoading {
  @override
  String toString() => "UserDataLoading";
}