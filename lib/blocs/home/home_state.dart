import 'package:equatable/equatable.dart';
import 'package:flutter_hemoterapia/models/donation.dart';
import 'package:meta/meta.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

// Is the initial state of the HomeForm
class HomeUninitialized extends HomeState {
  String toString() => 'HomeUninitialized';
}

// Is the state of the HomeForm when we are fetching data
class HomeLoading extends HomeState {
  String toString() => 'HomeLoading';
}

class HomeInitialized extends HomeState {
  final PersonalDonation personalDonation;
  const HomeInitialized({
    @required this.personalDonation,
  });

  String toString() => 'HomeInitialized';
}

// Is the state of the HomeForm when a fetch attempt has failed.
class HomeFailure extends HomeState {
  final String error;

  const HomeFailure({@required this.error}) : assert(error != null);

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'HomeFailure {error: $error}';
}
