import 'package:bloc/bloc.dart';
import 'package:flutter_hemoterapia/blocs/register/register_event.dart';
import 'package:flutter_hemoterapia/blocs/register/register_state.dart';
import 'package:flutter_hemoterapia/blocs/signup/signup_bloc.dart';
import 'package:flutter_hemoterapia/blocs/signup/signup_event.dart';
import 'package:flutter_hemoterapia/repository/user_repository.dart';
import 'package:meta/meta.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository userRepository;
  final SignupBloc signupBloc;
  RegisterBloc({@required this.userRepository, @required this.signupBloc})
      : assert(userRepository != null);

  @override
  RegisterState get initialState => RegisterInitial();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterUserButtonPressed) {
      yield RegisterLoading();
      try {
        String token = await userRepository.registerUser(
          email: event.email,
          password: event.password,
        );
        signupBloc.add(UserRegistered(token: token));
        yield RegisterInitial();
      } catch (error) {
        yield RegisterFailure(error: error);
      }
    }

    if (event is UserdataButtonPressed) {
      yield UserdataLoading();
      try {
        await userRepository.registerUserdata(person: event.person);

        signupBloc.add(UserdataEntered(person: event.person));

        yield UserdataInitial();
      } catch (error) {
        yield UserdataFailure(error: error);
      }
    }
  }
}
