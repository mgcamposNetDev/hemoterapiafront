import 'package:equatable/equatable.dart';

class RecoveryEmailState extends Equatable {
  @override
  List<Object> get props => [];
}

class RecoveryEmailUninitialized extends RecoveryEmailState {
  @override
  String toString() => 'RecoveryEmailUninitialized';
}

class RecoveryEmailLoading extends RecoveryEmailState {
  @override
  String toString() => 'RecoveryEmailLoading';
}

class RecoveryEmailInitialized extends RecoveryEmailState {
  @override
  String toString() => 'RecoveryEmailInitialized';
}

class RecoveryEmailFailured extends RecoveryEmailState {
  final String error;

  RecoveryEmailFailured(this.error);

  @override
  String toString() => 'RecoveryEmailFailured ${this.error}';
}

class RecoveryEmailSuccess extends RecoveryEmailState {
  RecoveryEmailSuccess();

  @override
  String toString() => 'RecoveryEmailSuccess';
}