import 'package:bloc/bloc.dart';
import 'package:flutter_hemoterapia/blocs/recoveryemail/recovery_email_event.dart';
import 'package:flutter_hemoterapia/blocs/recoveryemail/recovery_email_state.dart';

class RecoveryEmailBloc extends Bloc<RecoveryEmailEvent, RecoveryEmailState> {
  @override
  RecoveryEmailState get initialState => RecoveryEmailUninitialized();

  @override
  Stream<RecoveryEmailState> mapEventToState(
    RecoveryEmailEvent event,
  ) async* {
    if (event is RecoveryEmailButtonPressed) {
      yield* _mapRecoveryEmailButtonPressedToState(event);
    }

    if (event is RecoveryEmailReset) {
      yield* _mapRecoveryEmailResetToState(event);
    }
  }

  Stream<RecoveryEmailState> _mapRecoveryEmailButtonPressedToState(
      RecoveryEmailButtonPressed event) async* {
    try {
      yield RecoveryEmailSuccess();
    } catch (error) {
      yield RecoveryEmailFailured(error);
    }
  }

  Stream<RecoveryEmailState> _mapRecoveryEmailResetToState(
      RecoveryEmailEvent event) async* {
      yield RecoveryEmailUninitialized();
  }
}
