import 'package:bloc/bloc.dart';
import 'package:flutter_hemoterapia/blocs/authentication/authentication_event.dart';
import 'package:flutter_hemoterapia/blocs/authentication/authentication_state.dart';
import 'package:flutter_hemoterapia/repository/user_repository.dart';
import 'package:meta/meta.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState>{
  final UserRepository userRepository;

  AuthenticationBloc({@required this.userRepository})
    : assert(userRepository != null);

  


  @override
  AuthenticationState get initialState => AuthenticationUnauthenticated();

  @override
  Stream<AuthenticationState> mapEventToState(AuthenticationEvent event) async* {

    if (event is AppStarted) {
      yield AuthenticationUninitialized();
      final bool hasToken = await userRepository.hasToken();
      if (hasToken) {
        final bool isValid = await userRepository.validateToken();
        if (isValid) {
          yield AuthenticationAuthenticated();
        } else {
          yield AuthenticationUnauthenticated();
          print("TOKEN NO VALIDO");
        }
      } else {
        yield AuthenticationUnauthenticated();
      }  
    }

    if (event is LoggedIn) {
      yield AuthenticationLoading();
      await userRepository.persistToken(event.token);
      bool firstTime = await userRepository.isFirstLogin();
      if (firstTime) {
        await userRepository.persistFirstLogin();
        yield AuthenticationAuthenticatedFirstTime(); 
      } else {
        yield AuthenticationAuthenticated();
      } 
    }

    if (event is LoggedOut) {
      yield AuthenticationLoading();
      await userRepository.deleteToken();
      yield AuthenticationUnauthenticated();
    }
  }
}