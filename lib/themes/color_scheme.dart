import 'package:flutter/material.dart';

class ThemeColors {
  static Color primary = const Color(0xFF223c62);
  static Color primaryDark = const Color(0xFF172a44);
  static Color primaryLight = const Color(0xFF4e6381);
  static Color secondary = const Color(0xFFc9000d);
  static Color secondaryDark = const Color(0xFF8c0009);
  static Color secondaryLight = const Color(0xFFd3333d);
  static Color white = const Color(0xFFfdfdfd);
  static Color white60 = const Color(0x99fdfdfd);
  static Color disable = const Color(0xFFc1c1c1);
}