import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/themes/input.dart';
import 'color_scheme.dart';

ThemeData getDefaultTheme() {
  return ThemeData(
    primaryColor: ThemeColors.primary,
    primaryColorDark: ThemeColors.primaryDark,
    primaryColorLight: ThemeColors.primaryLight,
    accentColor: ThemeColors.white,
    inputDecorationTheme: outlineInputTheme(),
    buttonTheme: ButtonThemeData(
      minWidth: double.infinity,
    ),
    textTheme: TextTheme(
      body1: new TextStyle(color: ThemeColors.white),
      body2: new TextStyle(color: ThemeColors.white),
      button: new TextStyle(color: ThemeColors.white),
      caption: new TextStyle(color: ThemeColors.white),
      display1: new TextStyle(color: ThemeColors.white),
      display2: new TextStyle(color: ThemeColors.white),
      display3: new TextStyle(color: ThemeColors.white),
      display4: new TextStyle(color: ThemeColors.white),
      headline: new TextStyle(color: ThemeColors.white),
      subhead: new TextStyle(color: ThemeColors.white),
      title: new TextStyle(color: ThemeColors.white),
    ),
    canvasColor: ThemeColors.primaryDark,
  );
}

ThemeData getDatePickerTheme() {
  return ThemeData(
    primaryColor: ThemeColors.primary,
    primaryColorDark: ThemeColors.primaryDark,
    accentColor: ThemeColors.secondary,
    primaryColorLight: ThemeColors.primaryLight,
    canvasColor: ThemeColors.primary,
    cursorColor: ThemeColors.primary,
    cardColor: ThemeColors.primary,
    primarySwatch: MaterialColor(
      ThemeColors.primary.value,
      const <int, Color>{
        50: const Color(0xFFE4E8EC),
        100: const Color(0xFFBDC5D0),
        200: const Color(0xFF919EB1),
        300: const Color(0xFF647791),
        400: const Color(0xFF43597A),
        500: const Color(0xFF223C62),
        600: const Color(0xFF1E365A),
        700: const Color(0xFF192E50),
        800: const Color(0xFF142746),
        900: const Color(0xFF0C1A34),
      },
    ),
  );
}
