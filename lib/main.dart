import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/authentication/authentication_bloc.dart';
import 'package:flutter_hemoterapia/blocs/authentication/authentication_event.dart';
import 'package:flutter_hemoterapia/blocs/signup/signup_bloc.dart';
import 'package:flutter_hemoterapia/repository/donation_repository.dart';
import 'package:flutter_hemoterapia/repository/person_repository.dart';
import 'package:flutter_hemoterapia/repository/user_repository.dart';
import 'package:flutter_hemoterapia/screens/drawer.dart';
import 'package:flutter_hemoterapia/screens/login/login.dart';
import 'package:flutter_hemoterapia/screens/not_found.dart';
import 'package:flutter_hemoterapia/screens/recovery/recovery_code.dart';
import 'package:flutter_hemoterapia/screens/recovery/recovery_password.dart';
import 'package:flutter_hemoterapia/screens/recovery/recovery_user.dart';
import 'package:flutter_hemoterapia/screens/register/register.dart';
import 'package:flutter_hemoterapia/screens/register/userdata.dart';
import 'package:flutter_hemoterapia/screens/slider.dart';
import 'package:flutter_hemoterapia/screens/splash.dart';
import 'package:flutter_hemoterapia/themes/theme.dart';
import 'package:meta/meta.dart';
import 'dart:async';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}

Future<void> main() async {
  // Ensure that plugin services are initialized so that `availableCameras()`
  // can be called before `runApp()`
  WidgetsFlutterBinding.ensureInitialized();

  BlocSupervisor.delegate = SimpleBlocDelegate();
  final userRepository = UserRepository();
  final donationsRepository = DonationsRepository();
  final personRepository = PersonRepository();

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticationBloc>(
          create: (context) =>
              AuthenticationBloc(userRepository: userRepository)
                ..add(AppStarted()),
        ),
        BlocProvider<SignupBloc>(
          create: (context) => SignupBloc(userRepository: userRepository),
        ),
      ],
      child: App(userRepository: userRepository, personRepository: personRepository, donationsRepository: donationsRepository,),
    ),
  );
}

class App extends StatelessWidget {
  final UserRepository userRepository;
  final PersonRepository personRepository;
  final DonationsRepository donationsRepository;

  App({Key key, @required this.userRepository, @required this.personRepository, @required this.donationsRepository }) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: getDefaultTheme(),
      initialRoute: '',
      routes: <String, WidgetBuilder>{
        "/": (BuildContext context) => SplashScreen(),
        "/login": (BuildContext context) =>
            LoginScreen(userRepository: userRepository),
        "/register": (BuildContext context) =>
            RegisterScreen(userRepository: userRepository),
        "/register_profile_data": (BuildContext context) =>
            RegisterUserDataScreen(userRepository: userRepository),
        "/recovery_user": (BuildContext context) => RecoveryUserScreen(),
        "/recovery_code": (BuildContext context) => RecoveryCodeScreen(),
        "/recovery_new_password": (BuildContext context) =>
            RecoveryNewPasswordScreen(),
        "/walkthrough": (BuildContext context) => WalkthroughScreen(),
        "/home": (BuildContext context) => DrawerScreen(personRepository: personRepository, donationsRepository: donationsRepository,)
      },
      onUnknownRoute: (RouteSettings setting) {
        return new MaterialPageRoute(
          builder: (context) => NotFoundScreen(),
        );
      },
    );
  }
}
