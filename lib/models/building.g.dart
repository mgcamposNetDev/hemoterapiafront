// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'building.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Building _$BuildingFromJson(Map<String, dynamic> json) {
  return Building(
    name: json['name'] as String,
    address: json['address'] as String,
    distance: (json['distance'] as num)?.toDouble(),
    x: json['x'] as double,
    y: json['y'] as double,
    id: json['id'] as int,
    hours: json['hours'] as String,
    phone: json['phone'] as String
  );
}

Map<String, dynamic> _$BuildingToJson(Building instance) => <String, dynamic>{
      'name': instance.name,
      'address': instance.address,
      'distance': instance.distance,
    };
