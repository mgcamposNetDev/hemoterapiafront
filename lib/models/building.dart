import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'building.g.dart';

@JsonSerializable()
class Building {
  int id;
  String name;
  String address;
  double distance;
  double x;
  double y;
  String hours;
  String phone;

  Building({@required this.id, @required this.name, @required this.address, @required this.distance, @required this.x, @required this.y, this.hours, this.phone});

  factory Building.fromJson(Map<String, dynamic> json) =>
      _$BuildingFromJson(json);
  Map<String, dynamic> toJson() => _$BuildingToJson(this);
}