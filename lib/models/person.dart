class Person {
  int id;
  String name;
  String surname;
  String documentType;
  String document;
  String dateBirth;
  String sex;
  String weigth;
  String profileUrl;
  String bloodType;
  String phone;
  String country;
  String province;
  String department;
  String address;
  String postalCode;

  Person({
    this.id,
    this.name,
    this.surname,
    this.documentType,
    this.document,
    this.dateBirth,
    this.sex,
    this.weigth,
    this.profileUrl,
    this.bloodType,
    this.phone,
    this.country,
    this.province,
    this.department,
    this.address,
    this.postalCode,
  });

  Person.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    surname = json['surname'];
    document = json['document'];
    documentType = json['document_type'];
    dateBirth = json['date_birth'];
    sex = json['sex'];
    weigth = json['weigth'];
    profileUrl = json['profile_url'];
    bloodType = json['blood_type'];
    country = json['country'];
    province = json['province'];
    department = json['department'];
    address = json['address'];
    postalCode = json['postal_code'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['surname'] = this.surname;
    data['document'] = this.document;
    data['date_birth'] = this.dateBirth;
    data['sex'] = this.sex;
    data['weigth'] = this.weigth;
    data['profile_url'] = this.profileUrl;
    data['blood_type'] = this.bloodType;
    data['country'] = this.country;
    data['province'] = this.province;
    data['department'] = this.department;
    data['address'] = this.address;
    data['postal_code'] = this.postalCode;
    data['phone'] = this.phone;
    data['document_type'] = this.documentType;

    return data;
  }
}
