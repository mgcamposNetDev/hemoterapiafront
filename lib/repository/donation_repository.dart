import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_hemoterapia/models/donation.dart';

class DonationsRepository {
  Future<PersonalDonation> getPersonDonation() async {
    await Future.delayed(Duration(seconds: 1));
    String jsonResponse = await rootBundle.loadString('assets/donations.json');
    final jsonDecode = json.decode(jsonResponse);
    PersonalDonation donationData = new PersonalDonation.fromJson(jsonDecode);
    print(donationData);
    return donationData;
  }
}