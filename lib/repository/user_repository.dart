import 'package:flutter/widgets.dart';
import 'package:flutter_hemoterapia/models/person.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserRepository {
  Future<String> authenticate({
    @required String email,
    @required String password,
  }) async {
    /// TODO: Implementar endpoint autenticacion
    await Future.delayed(Duration(seconds: 1));
    return 'token';
  }

  Future<void> deleteToken() async {
    /// delete from keystore/keychain
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove("token");
    return;
  }

  Future<void> persistToken(String token) async {
    /// write to keystore/keychain
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("token", token);
    return;
  }

  Future<bool> hasToken() async {
    /// read from keystore/keychain
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    return token != null ? token.isNotEmpty : false;
  }

  Future<String> getToken() async {
    /// read from keystore/keychain
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("token");
  }

  Future<bool> validateToken() async {
    /// TODO: Implementar endpoint validar token
    await getToken();
    await Future.delayed(Duration(seconds: 5));
    return true;
  }

  Future<bool> isFirstLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      bool initialized = prefs.getBool("initialized");
      if (initialized) {
        return false;
      }
      return true;
    } catch (e) {
      return true;
    }
  }

  Future<void> persistFirstLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("initialized", true);
  }

  Future<String> registerUser({
    @required String email,
    @required String password,
  }) async {
    await Future.delayed(Duration(seconds: 1));
    return 'token';
  }

  Future<String> registerUserdata({
    @required Person person,
  }) async {
    await Future.delayed(Duration(seconds: 1));
    return 'ok';
  }
}
