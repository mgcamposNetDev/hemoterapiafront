import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_hemoterapia/models/person.dart';

class PersonRepository {
  Future<Person> getPersonalData() async {
    await Future.delayed(Duration(seconds: 1));
    String jsonResponse = await rootBundle.loadString('assets/personal_data.json');
    final jsonDecode = json.decode(jsonResponse);
    Person person = new Person.fromJson(jsonDecode);
    print(person);
    return person;
  }

  Future<Person> savePersonalData() async {
    await Future.delayed(Duration(seconds: 1));
    String jsonResponse = await rootBundle.loadString('assets/personal_data.json');
    final jsonDecode = json.decode(jsonResponse);
    Person person = new Person.fromJson(jsonDecode);
    return person;
  }
}