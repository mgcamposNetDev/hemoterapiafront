import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:progress_dialog/progress_dialog.dart';

ProgressDialog loading(BuildContext context) {
  var pr = new ProgressDialog(
    context,
    type: ProgressDialogType.Normal,
    isDismissible: false,
    showLogs: true,
  );

  pr.style(
    message: 'Cargando...',
    borderRadius: 10.0,
    backgroundColor: Theme.of(context).primaryColor,
    progressWidget: LoadingLogo(),
    elevation: 10.0,
    insetAnimCurve: Curves.easeInOut,
    progress: 0.0,
    maxProgress: 100.0,
    progressTextStyle: TextStyle(
      color: Colors.white,
      fontSize: 13.0,
      fontWeight: FontWeight.w400,
    ),
    messageTextStyle: TextStyle(
      color: Colors.white,
      fontSize: 19.0,
      fontWeight: FontWeight.w600,
    )
  );

  return pr;
}

class LoadingLogo extends StatefulWidget {
  @override
  _LoadingLogoState createState() => _LoadingLogoState();
}

class _LoadingLogoState extends State<LoadingLogo>
    with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(milliseconds: 1600),
      vsync: this,
    );
    _controller.repeat();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
        turns: Tween(begin: 0.0, end: 1.0).animate(_controller),
        child: new SvgPicture.asset('assets/images/logo.svg',
            semanticsLabel: 'logo'));
  } // it starts the animation
}
